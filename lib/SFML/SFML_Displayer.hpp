#ifndef SFML_DISPLAYER_HPP
#define SFML_DISPLAYER_HPP

#include "../../incs/IDisplayer.hpp"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include <menu.h>
#include <map>
#include <sys/param.h>



#define TEXTURE_WIDTH 16
#define TEXTURE_HEIGHT 16
#define TILE_SIZE 16

#define NBR_ERRORS_POSSIBLE 18

class SFML_Displayer : public IDisplayer{
	private:
		sf::RenderWindow			*_window;
		sf::Texture 				_images[40];
		sf::Sprite					_food;
		sf::Sprite					_walls;
		sf::Sprite					_background;
		sf::Sprite					_snake[14];
		sf::Sprite					_numbers[13];
		sf::Sprite					_score[6];
		sf::Sprite					_menu;
		bool						_menu_load;


		std::map <int, eKey>		key_tab;
		size_t						_height;
		size_t						_width;
		std::list<Point>			_old_snake;
		std::list<Point>			_old_food;
		std::string					_window_size_error;

	public:
						SFML_Displayer();
						SFML_Displayer(SFML_Displayer const & src);
		virtual 		~SFML_Displayer();
		SFML_Displayer	&operator=(const SFML_Displayer & rhs);
		void			init(int height, int width);
		void			display_all();
		void			close();
		void			clear_window();
		int				display_menu(std::string *menu, int size);
		void			draw_str_score();
		void			insert_score(std::string score);
		void			insert_clock(std::string clock);
		void			insert_snake(std::list<SnakeObject> snake);
		void			delete_food(std::list<Point> food);
		void			insert_food(std::list<Point> food);
		void			insert_walls(std::list<Point> walls);
		void			display_banner();
		eKey			get_input() const;
		bool			window_size_ok(int height, int width);
		std::string		getResourcePath(const std::string & asset);
		void			create_message_window_error_size(sf::VideoMode DM);
		void			load_food();
		void			draw_background();
		void			draw_one_background_tile(int x, int y);
		void			load_walls();
		void			draw_walls_around_map();
		void			load_background();
		void			load_snake_heads();
		void			load_snake_tails();
		void			load_snake_body();
		void			load_numbers();
		void			load_score();
		void			load_menu();
		std::map <int, eKey>		get_key_tab() const;
		size_t						get_height() const;
		size_t						get_width() const;
		std::list<Point>			get_old_snake() const;
		std::list<Point>			get_old_food() const;
		std::string					get_window_size_error() const;
		sf::RenderWindow			*get_window() const;
		sf::Sprite					get_food() const;
		sf::Sprite					get_walls() const;
		sf::Sprite					get_background() const;
		sf::Sprite					get_menu() const;


	class Except_SFML_Displayer : public std::exception {
		private:
			std::string		_msg;
			std::string		_errors[NBR_ERRORS_POSSIBLE];
			unsigned int	_n;
							Except_SFML_Displayer();
		public:
							Except_SFML_Displayer(unsigned int n, std::string msg);
							Except_SFML_Displayer(const Except_SFML_Displayer & src);
		virtual				~Except_SFML_Displayer() _NOEXCEPT;
		Except_SFML_Displayer	&		operator=(const Except_SFML_Displayer & rhs);
		virtual const char * what() const throw();
	};
};

std::ostream &			operator<<(std::ostream & o, SFML_Displayer::Except_SFML_Displayer const & i);

#endif
