#ifndef SDL_DISPLAYER_HPP
#define SDL_DISPLAYER_HPP

#include "../../incs/IDisplayer.hpp"
#include "cleanup.template.hpp"

#include <stdlib.h>
#include <stdio.h>
#include "include/SDL2/SDL.h"
#include <menu.h>
#include <map>



#define TEXTURE_WIDTH 16
#define TEXTURE_HEIGHT 16
#define TILE_SIZE 16

#define NBR_ERRORS_POSSIBLE 18

class SDL_Displayer : public IDisplayer{
	private:
		SDL_Window				*_win;
		SDL_Renderer			*_renderer;
		SDL_Texture 			*_food;
		SDL_Texture				*_walls;
		SDL_Texture				*_background;
		SDL_Texture				*_snake[14];
		SDL_Texture				*_numbers[13];
		SDL_Texture				*_score[6];
		SDL_Texture				*_menu;
		bool					_menu_load;


		std::map <int, eKey>	key_tab;
		size_t					_height;
		size_t					_width;
		std::list<Point>		_old_snake;
		std::list<Point>		_old_food;
		std::string				_window_size_error;

	public:
					SDL_Displayer();
		virtual 	~SDL_Displayer();
					SDL_Displayer(const SDL_Displayer & src);

		SDL_Displayer & operator=(const SDL_Displayer & src);

		void			init(int height, int width);
		void			display_all();
		void			close();
		void			clear_window();
		int				display_menu(std::string *menu, int size);
		void			draw_str_score();
		void			insert_score(std::string score);
		void			insert_clock(std::string clock);
		void			insert_snake(std::list<SnakeObject> snake);
		void			delete_food(std::list<Point> food);
		void			insert_food(std::list<Point> food);
		void			insert_walls(std::list<Point> walls);
		void			display_banner();
		eKey			get_input() const;
		void 			renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y);
		void 			renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, int texture_width, int texture_height);
		bool			window_size_ok(int height, int width);
		SDL_Texture*	loadTexture(const std::string &file, SDL_Renderer *ren);
		std::string		getResourcePath(const std::string & asset);
		void			create_message_window_error_size(SDL_DisplayMode DM);
		void			load_food();
		void			draw_background();
		void			draw_one_background_tile(int x, int y);
		void			load_walls();
		void			draw_walls_around_map();
		void			load_background();
		void			load_snake_heads();
		void			load_snake_tails();
		void			load_snake_body();
		void			load_numbers();
		void			load_score();
		void			load_menu();
		SDL_Window				*get_win() const;
		SDL_Renderer			*get_renderer() const;
		SDL_Texture 			*get_food() const;
		SDL_Texture				*get_walls() const;
		SDL_Texture				*get_background() const;
		SDL_Texture				*get_snake() const;
		SDL_Texture				*get_numbers() const;
		SDL_Texture				*get_score() const;
		SDL_Texture				*get_menu() const;


		std::map <int, eKey>	get_key_tab() const;
		size_t					get_height() const;
		size_t					get_width() const;
		std::list<Point>		get_old_snake() const;
		std::list<Point>		get_old_food() const;
		std::string				get_window_size_error() const;

	class Except_SDL_Displayer : public std::exception {
		private:
			std::string		_msg;
			std::string		_errors[NBR_ERRORS_POSSIBLE];
			unsigned int	_n;
							Except_SDL_Displayer();
		public:
							Except_SDL_Displayer(unsigned int n, std::string msg);
							Except_SDL_Displayer(const Except_SDL_Displayer & src);
		virtual				~Except_SDL_Displayer() _NOEXCEPT;
		Except_SDL_Displayer	&		operator=(const Except_SDL_Displayer & rhs);
		virtual const char * what() const throw();
	};
};

std::ostream &			operator<<(std::ostream & o, SDL_Displayer::Except_SDL_Displayer const & i);

#endif
