#include "Displayer_factory.hpp"

Displayer_factory::Displayer_factory(){
}

Displayer_factory::Displayer_factory(int win_height, int win_width, int lib): _lib(NULL), _handle(NULL){
	this->_lib_tab[0] = "./lib/ncurses.so";
	this->_lib_tab[1] = "./lib/sdl.so";
	this->_lib_tab[2] = "./lib/sfml.so";

	_log << _lib_tab[lib -1] << std::endl;
	try{
		link_lib(_lib_tab[lib -1], win_height, win_width);
	}catch(std::exception const & e){
		std::cerr << e.what() << std::endl;
		throw Except_factory(0, _lib_tab[lib -1]);
	}
}

Displayer_factory::~Displayer_factory(){
	dlclose(_handle);
}

void	Displayer_factory::link_lib(std::string const & path,  int win_height, int win_width){
	char *error;
	void *(*lib_loader)(void);

	if(_lib != NULL)
		delete_lib();

	dlerror();
	_handle = dlopen(path.c_str(), RTLD_NOW);
	if (!_handle) {
		fputs (dlerror(), stderr);
		throw Except_factory(1, path);
	}

	*reinterpret_cast<void**>(&lib_loader) = dlsym(_handle, "lib_loader");
	if ((error = dlerror()) != NULL)  {
		fputs(error, stderr);
		throw Except_factory(1, path);
	}

	_lib = reinterpret_cast<IDisplayer *>(lib_loader());

	_lib->init(win_height, win_width);
}

void	Displayer_factory::switch_lib(int win_height, int win_width, int new_lib){
	try{
		link_lib(_lib_tab[new_lib -1], win_height, win_width);
	}catch(std::exception const & e){
		std::cerr << e.what() << std::endl;
		throw Except_factory(0, _lib_tab[new_lib -1]);
	}
}

void	Displayer_factory::delete_lib(){
	_lib->clear_window();
	close();
	_lib = NULL;
	_handle = NULL;
}

int		Displayer_factory::display_menu(std::string *menu, int size) {
	return (_lib->display_menu(menu, size));
}

void	Displayer_factory::insert_score(std::string score){
	_lib->insert_score(score);
}
void	Displayer_factory::insert_clock(std::string clock){
	_lib->insert_clock(clock);
}
void	Displayer_factory::insert_snake(std::list<SnakeObject> snake){
	_lib->insert_snake(snake);
}
void	Displayer_factory::insert_food(std::list<Point> food){
	_lib->insert_food(food);
}
void	Displayer_factory::insert_walls(std::list<Point> walls){
	_lib->insert_walls(walls);
}

eKey	Displayer_factory::get_input() const {
	return (_lib->get_input());
}

void	Displayer_factory::close(){
	_lib->close();
	dlclose(_handle);
}

void	Displayer_factory::clear_window(){
	_lib->clear_window();
}

void	Displayer_factory::display_all(){
	_lib->display_all();
}

/*
** Exception class
*/

Displayer_factory::Except_factory::Except_factory(unsigned int n, std::string msg) : _n(n){
		if (DEBUGG <= 1)
			_log << "Except_factory constructor called" << std::endl;
	_errors[0] = static_cast<std::string>(" : Link of graphic library failed.");
	_errors[1] = static_cast<std::string>(" : Initialization of graphic library failed.");
	_errors[n].insert(0, msg);
}

Displayer_factory::Except_factory::Except_factory(const Except_factory & src){
		if (DEBUGG <= 1)
			_log << "Except_factory copy constructor called" << std::endl;
	static_cast<void>(src);
}

Displayer_factory::Except_factory::~Except_factory() throw(){

}

Displayer_factory::Except_factory & Displayer_factory::Except_factory::operator=(const Except_factory & rhs){

	static_cast<void>(rhs);
	return *this;
}

const char *Displayer_factory::Except_factory::what() const throw(){
	return (_errors[_n].c_str());
}

std::ostream &			operator<<(std::ostream & o, Displayer_factory::Except_factory const & i){
	o << i.what();
	return o;
}
