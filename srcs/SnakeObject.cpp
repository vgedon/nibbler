#include <SnakeObject.hpp>

SnakeObject::SnakeObject(): _asset(0) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject constructor called" << std::endl;
}

SnakeObject::SnakeObject(const Point & src): Point(src), _asset(0) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject constructor called" << std::endl;
}

SnakeObject::SnakeObject(const Point & src, int asset): Point(src), _asset(asset) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject constructor called" << std::endl;
}

SnakeObject::SnakeObject(const SnakeObject & src) : Point(src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject constructor called" << std::endl;
	_asset = src.getAsset();
}

SnakeObject &		SnakeObject::operator=(const SnakeObject & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject constructor called" << std::endl;
	_x = src.getX();
	_y = src.getY();
	_asset = src.getAsset();
	return *this;
}

SnakeObject::~SnakeObject(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject destructor called" << std::endl;
}

std::ostream &	operator<<(std::ostream & o, SnakeObject const & e){
	o << "SnakeObject(" << e.getX() << "," << e.getY() << ")";
	return o;
}

/*
	Methods
*/

void		SnakeObject::setAsset(int asset){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject asset setter called" << std::endl;
	_asset = asset;
}

int			SnakeObject::getAsset() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeObject asset getter called" << std::endl;
	return _asset;
}