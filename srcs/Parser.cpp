#include <Parser.hpp>
#include <iostream>

void	parse(const char **argv, int argc, tInterface * interface){

	try{

		if (argc > 2 && argc < 5)
		{
			interface->width = std::stoi(argv[1]);
			interface->height = std::stoi(argv[2]);
			if (interface->width < 10 || interface->height < 10)
				throw ExceptParser("Edges of the map should have a minimum value of 10");
			if (argv[3]){
				interface->lib = std::stoi(argv[3]);
				if (interface->lib < 1 || interface->lib > 3)
					throw ExceptParser("graphics libraries codes are between 1 and 3");
			}else
			{
				interface->lib = 1;
			}
		}else
			throw ExceptParser((std::string("Usage: ")+argv[0]+" width height [code of graphic library (1-3) ]").c_str());
	}catch(std::invalid_argument const &e){
		std::cerr << "Arguments must be valid positive number." << std::endl;
	}
	catch(std::out_of_range const &e){
		std::cerr << "Arguments must be valid positive integer." << std::endl;
	}
	catch(std::exception const &e){
		std::cerr << e.what() << std::endl;
	}
}

ExceptParser::ExceptParser() :  _msg("unknow reason"){
	if (DEBUGG <= 1)
		_log << "[Debug:1] ExceptParser constructor called" << std::endl;
}

ExceptParser::ExceptParser(std::string msg) :  _msg(msg){
	if (DEBUGG <= 1)
		_log << "[Debug:1] ExceptParser constructor called" << std::endl;
}

ExceptParser::ExceptParser(const ExceptParser & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] ExceptParser copy constructor called" << std::endl;
	static_cast<void>(src);
}

ExceptParser::~ExceptParser() throw(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] ExceptParser destructor called" << std::endl;
}

ExceptParser & ExceptParser::operator=(const ExceptParser & rhs){
	if (DEBUGG <= 1)
		_log << "[Debug:1] ExceptParser operator = called" << std::endl;

	static_cast<void>(rhs);
	return *this;
}

const char *ExceptParser::what() const throw(){
	return (_msg.c_str());
}

std::ostream &			operator<<(std::ostream & o, ExceptParser const & i){
	o << i.what();
	return o;
}
