#include <SnakeHandler.hpp>

SnakeHandler::SnakeHandler(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler constructor called" << std::endl;
}

SnakeHandler::SnakeHandler(tInterface & interface){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler constructor called" << std::endl;
	Point	start(interface.width/2, interface.height/2);

	_snakeBody.push_back(*(new SnakeObject(start, HEAD_RIGHT)));
	_snakeBody.push_back(*(new SnakeObject(Point (interface.width/2 -1, interface.height/2), BODY_HORIZONTAL)));
	_snakeBody.push_back(*(new SnakeObject(Point (interface.width/2 -2, interface.height/2), BODY_HORIZONTAL)));
	_snakeBody.push_back(*(new SnakeObject(Point (interface.width/2 -3, interface.height/2), TAIL_LEFT)));
}

SnakeHandler::SnakeHandler(const SnakeHandler & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler constructor called" << std::endl;
	const std::list<SnakeObject> & l = src.getSnakeBody();

	for (std::list<SnakeObject>::const_iterator i = l.begin(); i != l.end(); ++i)
		_snakeBody.push_back(*i);
}

SnakeHandler &		SnakeHandler::operator=(const SnakeHandler & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler constructor called" << std::endl;
	const std::list<SnakeObject> & l = src.getSnakeBody();

	for (std::list<SnakeObject>::const_iterator i = l.begin(); i != l.end(); ++i)
		_snakeBody.push_back(*i);
	return *this;
}

SnakeHandler::~SnakeHandler(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler destructor called" << std::endl;
}

std::ostream &	operator<<(std::ostream & o, SnakeHandler const & e){
	const std::list<SnakeObject> & l = e.getSnakeBody();

	o << "SnakeHandler. SnakeBody is " << l.size() << " long. H";
	for (std::list<SnakeObject>::const_iterator i = l.begin(); i != l.end(); ++i)
		o << "[" << (*i).getX() << ":" << (*i).getY() << "|" << (*i).getAsset() << "] ";
	return o;
}

/*
	Methods
*/

const std::list<SnakeObject> & 		SnakeHandler::getSnakeBody() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler snakeBody getter called" << std::endl;
	return _snakeBody;
}


const std::list<Point> & 		SnakeHandler::getSnakeBody(int) const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler snakeBody getter called" << std::endl;
	std::list<Point> *snakeBody = new std::list<Point>;
	for (std::list<SnakeObject>::const_iterator bodyPart = _snakeBody.begin(); bodyPart != _snakeBody.end(); ++bodyPart)
			snakeBody->push_back(*bodyPart);
	return *snakeBody;
}

const SnakeObject & 		SnakeHandler::getSnakeHead() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] SnakeHandler snakeBody getter called" << std::endl;
	return _snakeBody.front();
}

Point &	SnakeHandler::computeDirection(){
	Point *t;
	t = new Point (_snakeBody.front() - *(++(_snakeBody.begin())));

	if (DEBUGG <= 2)
		_log << "[Debug:2] SnakeHandler: direction : " << *t << std::endl;
	return *t;
}

bool	SnakeHandler::isSnakeBody(Point & p){
	if (DEBUGG <= 2)
		_log << "[Debug:2] SnakeHandler: checking body collision." << std::endl;
		for (std::list<SnakeObject>::iterator bodyPart = ++(_snakeBody.begin()); bodyPart != _snakeBody.end(); ++bodyPart)
		{
			if (*bodyPart == p)
				return true;
		}
		return false;
}

void 	SnakeHandler::goForwardGrowing(Point & direction){
	Point	ahead = computeDirection();
	Point *next = NULL;
	if (Point(ahead + direction) != Point(0,0))
		next = new Point (_snakeBody.front() + direction);
	else
		*next = ahead;

	if (DEBUGG <= 4)
		_log << "[Debug:4] SnakeHandler: moving snakeHead to : " << *next << std::endl;
	_snakeBody.insert(_snakeBody.begin(), *next);
}

void	SnakeHandler::removeTail(){
	if (DEBUGG <= 2)
		_log << "[Debug:2] SnakeHandler: remove tail." << std::endl;
	_snakeBody.pop_back();
}

std::string	SnakeHandler::computeBodyOrientation(std::list<SnakeObject>::iterator bodyPart){
	std::string 		links = *new std::string("0000");

	Point before(*(--bodyPart) - *(++bodyPart));
	Point after(*(++bodyPart) - *(--bodyPart));
	links[0] = (before.getY() < 0 || after.getY() < 0) + '0';
	links[1] = (before.getY() > 0 || after.getY() > 0) + '0';
	links[2] = (before.getX() < 0 || after.getX() < 0) + '0';
	links[3] = (before.getX() > 0 || after.getX() > 0) + '0';

	return links;
}

void	SnakeHandler::updateAsset(){
	if (DEBUGG <= 3)
		_log << "[Debug:3] SnakeHandler: update assets head to tail => ";

	std::map<Point, int> headAsset;
	headAsset[Point(1,0)] = HEAD_RIGHT;
	headAsset[Point(-1,0)] = HEAD_LEFT;
	headAsset[Point(0,1)] = HEAD_UP;
	headAsset[Point(0,-1)] = HEAD_DOWN;

	Point		head = computeDirection();
	if (DEBUGG <= 3)
		_log << headAsset[head] << ":";
	_snakeBody.front().setAsset(headAsset[head]);

	std::map<std::string, int> bodyAsset;

	bodyAsset["1100"] = BODY_VERTICAL;
	bodyAsset["0011"] = BODY_HORIZONTAL;
	bodyAsset["1010"] = ANGLE_LEFT_UP;
	bodyAsset["1001"] = ANGLE_RIGHT_UP;
	bodyAsset["0110"] = ANGLE_LEFT_DOWN;
	bodyAsset["0101"] = ANGLE_RIGHT_DOWN;

	for (std::list<SnakeObject>::iterator bodyPart = ++(_snakeBody.begin()); bodyPart != --_snakeBody.end(); ++bodyPart) {
		if (DEBUGG <= 3)
		_log << bodyAsset[computeBodyOrientation(bodyPart)] << ":";
		bodyPart->setAsset(bodyAsset[computeBodyOrientation(bodyPart)]);
	}

	std::map<Point, int> tailAsset;
	tailAsset[Point(1,0)] = TAIL_LEFT;
	tailAsset[Point(-1,0)] = TAIL_RIGHT;
	tailAsset[Point(0,1)] = TAIL_DOWN;
	tailAsset[Point(0,-1)] = TAIL_UP;

	if (DEBUGG <= 3)
		_log << tailAsset[(*(--(--(_snakeBody.end()))) - _snakeBody.back())] << std::endl;
	_snakeBody.back().setAsset(tailAsset[(*(--(--(_snakeBody.end()))) - _snakeBody.back())]);
}

