#include <Nibbler.hpp>
#include <time.h>

std ::ofstream _log  ("nibbler.log", std::ofstream::out|std::ofstream::app);

int main(int argc, char const *argv[])
{
	/*
		As logs are append to same file
		create a header like start
		simplifying finding of different run
	*/
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	_log << "===============================================================================" << std::endl;
	_log << "new sesssion start : " << asctime (timeinfo) << std::endl;
	_log << "passed arguments : " ;
	for (int i = 1; i < argc; ++i)
	{
		_log << argv[i] << " ";
	}
	_log << std::endl;
	_log << "===============================================================================" << std::endl;


	GameHandler *game = new GameHandler(argc, argv);

	int		menuRet;
	if (game->isGood()){
		menuRet = game->gameMenu();
		if (menuRet == 0)
			game->gameLoop();
		delete(game);
	}

	return 0;
}