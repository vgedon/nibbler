#include <Timer.hpp>

Timer::Timer(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Timer constructor called" << std::endl;
}

Timer::Timer(const Timer & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Timer constructor called" << std::endl;
	_startTime = src.getStartTime();
}

Timer &		Timer::operator=(const Timer & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Timer constructor called" << std::endl;
	_startTime = src.getStartTime();
	return *this;
}

Timer::~Timer(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Timer destructor called" << std::endl;
}

std::ostream &	operator<<(std::ostream & o, Timer const & e){
	o << "Timer elapsed time : " << e.getElapsedTime();
	return o;
}

/*
	Methods
*/

const std::chrono::steady_clock::time_point	&	Timer::getStartTime() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] Timer startTime getter called" << std::endl;
	return _startTime;
}

void			Timer::start(){
	if (DEBUGG <= 2)
		_log << "[Debug:2] Timer start" << std::endl;
	_startTime = std::chrono::steady_clock::now();
}

std::string	&	Timer::getElapsedTime() const{
	if (DEBUGG <= 2)
		_log << "[Debug:2] Timer stringify elapsed time" << std::endl;
	std::chrono::steady_clock::time_point 	time = std::chrono::steady_clock::now();
	std::chrono::duration<int> 	time_span = std::chrono::duration_cast<std::chrono::duration<int> >(time - _startTime);
	std::stringstream res;
	if (time_span.count() >= 60 * 60)
		res << time_span.count()/60*60 << "h" << std::setfill('0') << std::setprecision(2);
	if (time_span.count() >= 60)
		res << (time_span.count() / 60) << "m" << std::setfill('0') << std::setprecision(2);
	if (time_span.count() < 60 * 60)
	res << time_span.count() % 60 << "s";
	return *new std::string(res.str());
}
