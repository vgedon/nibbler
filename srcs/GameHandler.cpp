#include <GameHandler.hpp>

GameHandler::GameHandler() : _appleLifeTime(APPLELIFETIME) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler constructor called" << std::endl;
}

GameHandler::GameHandler(int argc, const char **argv) : _appleLifeTime(APPLELIFETIME) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler constructor called" << std::endl;
	try {
		_interface.width = 0;
		_interface.height = 0;
		_interface.lib = 0;
		parse(argv, argc, &_interface);
	} catch (ExceptParser & e) {
		std::cerr << e.what() << std::endl;
	}
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler constructor parsing done" << std::endl;
	if (_interface.width != 0 && _interface.height != 0 && _interface.lib != 0){
		std::chrono::high_resolution_clock::time_point 			time = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> 	seed = std::chrono::duration_cast<std::chrono::duration<int> >(time - time.min());
		_rndGen.seed(seed.count());
		_appleRandomness = 10;
		_frameLimiter = 0.33f;
		if (DEBUGG <= 1)
			_log << "[Debug:1] GameHandler constructor variable init done" << std::endl;
		_appleHandler = new AppleHandler ();
		if (DEBUGG <= 1)
			_log << "[Debug:1] GameHandler constructor apple done" << std::endl;
		_snakeHandler = new SnakeHandler (_interface);
		if (DEBUGG <= 1)
			_log << "[Debug:1] GameHandler constructor snake done" << std::endl;
		_graphic = NULL;
		try {
			_graphic = new Displayer_factory(_interface.height, _interface.width, _interface.lib);
			if (DEBUGG <= 1)
				_log << "[Debug:1] GameHandler constructor graphics done" << std::endl;
		}catch(std::exception const & e){
			std::cerr << e.what() << std::endl;
		}
	}
}

GameHandler::GameHandler(const GameHandler & src) : _appleLifeTime(APPLELIFETIME) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler constructor called" << std::endl;
	static_cast<void>(src);
}

GameHandler::~GameHandler() {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler destructor called" << std::endl;
	if (_graphic != NULL){
			_graphic->close();
			delete (_graphic);
		}
	delete (_appleHandler);
	delete (_snakeHandler);
	_log << "[Debug:1] Game is ended. " << _score << " in " << _timer.getElapsedTime() << std::endl;
}

GameHandler & GameHandler::operator=(const GameHandler & src) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler copy constructor called" << std::endl;
	static_cast<void>(src);
	return *this;
}

std::ostream &	operator<<(std::ostream & o, GameHandler const & e){
	o << "Nibbler GameHandler. Map is "+std::to_string(e.getWidth())+" by "+std::to_string(e.getHeight());
	return o;
}

/*
	Methods
*/

bool			GameHandler::isGood() {
	return (_graphic != NULL && _interface.width != 0 && _interface.height != 0 && _interface.lib != 0);
}

int 			GameHandler::getWidth() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler width getter called" << std::endl;
	return _interface.width;
}

int 			GameHandler::getHeight() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandler height getter called" << std::endl;
	return _interface.height;
}

void		GameHandler::appleSpwaner(){
	if (DEBUGG <= 2)
		_log << "[Debug:2] GameHandler an apple has spwaned" << std::endl;
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribX(0, _interface.width);
	std::uniform_int_distribution<int> distribY(0, _interface.height);

	Point a(distribX(generator), distribY(generator));
	if(!_appleHandler->isThereAnApple(a) && !_snakeHandler->isSnakeBody(a))
		_appleHandler->addApple(a, _appleLifeTime);
}

void		GameHandler::movingUpdate(eKey & keyPressed){
	if (DEBUGG <= 3)
		_log << "[Debug:3] GameHandler snake moved." << std::endl;
	std::map<int, Point> 	direction;

	direction[key_undef] = _snakeHandler->computeDirection();
	direction[key_up] = *new Point(0, -1);
	direction[key_down] = *new Point(0, 1);
	direction[key_left] = *new Point(-1, 0);
	direction[key_right] = *new Point(1, 0);

	Point 		dir(0,0);
	dir = dir - _snakeHandler->computeDirection();
	if (direction[keyPressed] == dir)
	{
		_snakeHandler->goForwardGrowing(_snakeHandler->computeDirection());
		return ;
	}
	_snakeHandler->goForwardGrowing(direction[keyPressed]);
}

bool		GameHandler::isOutOfBounds(Point & p){
	if (DEBUGG <= 2)
		_log << "[Debug:2] GameHandler check snake collisions." << std::endl;
	return (p.getX() < 0 || p.getX() >= _interface.width || p.getY() < 0 || p.getY() >= _interface.height);
}

bool		GameHandler::collisionChecker(){
	if (DEBUGG <= 3)
		_log << "[Debug:3] GameHandler check snake collisions." << std::endl;
	Point x;
	x = _snakeHandler->getSnakeHead();
	if (isOutOfBounds(x)) {
		return true;
	}
	for (std::list<SnakeObject>::const_iterator bodypart = ++(_snakeHandler->getSnakeBody().begin()); bodypart != _snakeHandler->getSnakeBody().end(); ++bodypart)
	{
		if (x == *bodypart){
			return true;
		}
	}
	if (_appleHandler->isThereAnApple(x)){
		_appleHandler->eatTheApple(x);
		_score += _score.getScore() / 10;
	} else
		_snakeHandler->removeTail();
	/*
		TODO

		wall handled collision
	*/
	return false;
}

int		GameHandler::gameMenu(){
	if (DEBUGG <= 3)
		_log << "[Debug:3] GameHandler display Menu." << std::endl;
	std::string menu[2];
	menu[0] = "single player";
	menu[1] = "exit";
	if (_graphic != NULL)
		return _graphic->display_menu(menu, 2);
	else
		return (-1);
}

Point &		GameHandler::findRandomEmptyLocation() {
	Point 	*pos;
	std::uniform_int_distribution<int> xDistrib(0,_interface.width - 1);
	std::uniform_int_distribution<int> yDistrib(0,_interface.height - 1);

	pos = new Point(xDistrib(_rndGen), yDistrib(_rndGen));
	while (_snakeHandler->isSnakeBody(*pos) || _appleHandler->isThereAnApple(*pos))
		pos = new Point(xDistrib(_rndGen), yDistrib(_rndGen));

	if (DEBUGG <= 4)
		_log << "[Debug:4] GameHandler compute a random free location : " << *pos << std::endl;
	return *pos;
}

void		GameHandler::computeAppleSpwaningRate(){
	_appleRandomness = (log10(_score.getScore()) / log10(_interface.width * _interface.height)) * 10 + 10;
	if (DEBUGG <= 4)
		_log << "[Debug:4] GameHandler: adjusting apple spwaning rate, new is : " << _appleRandomness << std::endl;
}

void		GameHandler::gameLoop(){
	if (DEBUGG <= 3)
		_log << "[Debug:3] GameHandler: gameLoop started" << std::endl;
	eKey	keyPressed = key_undef;
	std::chrono::steady_clock::time_point 			time = std::chrono::steady_clock::now();
	
	_timer.start();
	while(_graphic != NULL){
		computeAppleSpwaningRate();
		std::uniform_int_distribution<int> distrib(0,_appleRandomness);
		if( distrib(_rndGen) == 0)
			_appleHandler->addApple(findRandomEmptyLocation(), _appleLifeTime);

		_appleHandler->updateApples();

		try{
			keyPressed = _graphic->get_input();
		}catch (std::exception & e){
			std::cerr << e.what() << std::endl;
			break;
		}
		if (DEBUGG <= 3){
			_log << *_snakeHandler << std::endl;
			_log << *_appleHandler << std::endl;
		}
		if (keyPressed <= key_right){
			try{
				movingUpdate(keyPressed);
				if (collisionChecker())
					break;
			}catch (std::exception & e){
				std::cerr << e.what() << std::endl;
				break;
			}
		} else if (keyPressed == key_one || keyPressed == key_two || keyPressed == key_three){
			if (keyPressed == key_one)
				_interface.lib = 1;
			else if (keyPressed == key_two)
				_interface.lib = 2;
			else if (keyPressed == key_three)
				_interface.lib = 3;
		//J'ai cassé tout ton code avec ce try catch :-( mais le switch te renvoi une exception que tu ne catches pas :'(
			try{
				_graphic->switch_lib(_interface.height, _interface.width, _interface.lib);
			
			}catch (std::exception & e){
				std::cerr  << e.what() << std::endl ;
				break;
			}

		} else if (keyPressed == key_quit){
			if (DEBUGG <= 3)
				_log << "[Debug:3] GameHandler : quiting" << std::endl;
			break;
		}
		_score++;
		try{
			_snakeHandler->updateAsset();
			_graphic->clear_window();
			_graphic->insert_snake(_snakeHandler->getSnakeBody());
			_graphic->insert_food(_appleHandler->getApples(0));
			_graphic->insert_walls(*new std::list<Point>);
			_graphic->insert_score(_score.formatScore(5));
			_graphic->insert_clock(_timer.getElapsedTime());
			_graphic->display_all();
		}catch (std::exception & e){
			std::cerr << e.what() << std::endl;
			break;
		}
		std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(std::chrono::steady_clock::now() - time);
		if (DEBUGG <= 3)
			_log << "[Debug:3] GameHandler: gameLoop wait till next frame : " << fmax(((_frameLimiter) - time_span.count()), .0) << " sec" << std::endl;
		usleep(fmax(((_frameLimiter) - time_span.count()) * 1000000, .0));
		time = std::chrono::steady_clock::now();
		_frameLimiter = 1 / fmax(1 + log10(10 * _score.getScore()), 1/100);
		if (DEBUGG <= 4)
			_log << "[Debug:4] GameHandler: gameLoop new frameRate : " << _frameLimiter << std::endl;
	}
	if (collisionChecker())
		std::cout << "Game Over ! ! !" << std::endl << "score : " << _score.formatScore(12) << " in " << _timer.getElapsedTime() << std ::endl;
}


/*
	GameHandlerException
*/


GameHandler::GameHandlerException::GameHandlerException() :  _msg("undefined error"), _line(0) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandlerException constructor called" << std::endl;
}

GameHandler::GameHandlerException::GameHandlerException(std::string msg) :  _msg(msg), _line(0) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandlerException constructor called" << std::endl;
}

GameHandler::GameHandlerException::GameHandlerException(std::string msg, unsigned int line) :  _msg(msg), _line(line) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandlerException constructor called" << std::endl;
}

GameHandler::GameHandlerException::GameHandlerException(const GameHandlerException & src) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandlerException copy constructor called" << std::endl;
	static_cast<void>(src);
}

GameHandler::GameHandlerException::~GameHandlerException() _NOEXCEPT{
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandlerException destructor called" << std::endl;
}

GameHandler::GameHandlerException & GameHandler::GameHandlerException::operator=(const GameHandlerException & src) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] GameHandlerException operator = called" << std::endl;

	static_cast<void>(src);
	return *this;
}

const char *GameHandler::GameHandlerException::what() const throw(){
	std::string		str;
	if (_line)
		str = "Error line("+std::to_string(_line)+") : "+_msg;
	else
		str = "Error : " + _msg;
	return (str.c_str());
}

std::ostream &	operator<<(std::ostream & o, GameHandler::GameHandlerException const & e){
	o << e.what();
	return o;
}
