#ifndef SNAKEOBJECT_CLASS
#define SNAKEOBJECT_CLASS

#include <Point.hpp>

class SnakeObject : public Point
{
private:
	int				_asset;

public:
					SnakeObject();
					SnakeObject(const Point & src);
					SnakeObject(const Point & src, int asset);
					SnakeObject(const SnakeObject & src);
	SnakeObject &	operator=(const SnakeObject & src);
	virtual 		~SnakeObject();

	void			setAsset(int asset);
	int				getAsset() const;
};

std::ostream &	operator<<(std::ostream & o, SnakeObject const & e);

#endif