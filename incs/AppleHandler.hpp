#ifndef APPLE_HANDLER
#define APPLE_HANDLER

#include <AppleObject.hpp>
#include <iostream>
#include <list>
#include <Point.hpp>
#include <define.hpp>

class AppleHandler
{
private:
	std::list<AppleObject>	_apples;
	// unsigned int 			_defaultLifeTime;


public:
										AppleHandler();
										AppleHandler(const AppleHandler & src);
	AppleHandler &						operator=(const AppleHandler & src);
	AppleHandler &						operator=(const Point & src);
	virtual 							~AppleHandler();

	void								updateApples();
	const std::list<AppleObject> &		getApples() const;
	const std::list<Point> &			getApples(int) const;
	void								addApple(Point & p, unsigned int lifetime);
	bool								isThereAnApple(const Point & src);
	void								eatTheApple(const Point & src);

	class AppleHandlerException : public std::exception
	{
	private:
		std::string				_msg;
		unsigned int 			_line;

	public:
								AppleHandlerException();
								AppleHandlerException(std::string msg);
								AppleHandlerException(std::string msg, unsigned int line);
								AppleHandlerException(const AppleHandlerException & src);
		virtual					~AppleHandlerException() _NOEXCEPT;
		AppleHandlerException &	operator=(const AppleHandlerException & rhs);
		virtual const char *	what() const throw();

	};
};

std::ostream &	operator<<(std::ostream & o, AppleHandler const & e);
std::ostream &	operator<<(std::ostream & o, AppleHandler::AppleHandlerException const & e);

#endif

/*
	TODO

	error handling
		empty list
*/