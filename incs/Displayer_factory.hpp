#ifndef DISLAYER_FACTORY_HPP
#define DISLAYER_FACTORY_HPP

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <iostream>
#include <define.hpp>
#include "../incs/IDisplayer.hpp"
#include "../incs/SnakeObject.hpp"

#define NBR_ERRORS_POSSIBLE 3

class Displayer_factory {
	private:
		IDisplayer		*_lib;
		std::string		_lib_tab[3];
		void			*_handle;
						Displayer_factory();

	public:
				Displayer_factory(int win_height, int win_width, int lib);
		virtual ~Displayer_factory();
		void	link_lib(std::string const & path, int win_height, int win_width);
		void	switch_lib(int win_height, int win_width, int new_lib);
		void	delete_lib();
		int 	display_menu(std::string *menu, int size);
		void	insert_score(std::string score);
		void	insert_clock(std::string clock);
		void	insert_snake(std::list<SnakeObject> snake);
		void	insert_food(std::list<Point> food);
		void	insert_walls(std::list<Point> walls);
		eKey	get_input() const;
		void	display_all();
		void	clear_window();
		void	close();

	class Except_factory : public std::exception {
		private:
			std::string		_msg;
			std::string		_errors[NBR_ERRORS_POSSIBLE];
			unsigned int	_n;
							Except_factory();
		public:
							Except_factory(unsigned int n, std::string msg);
							Except_factory(const Except_factory & src);
		virtual				~Except_factory() _NOEXCEPT;
		Except_factory	&		operator=(const Except_factory & rhs);
		virtual const char * what() const throw();
	};
};

std::ostream &			operator<<(std::ostream & o, Displayer_factory::Except_factory const & i);

#endif