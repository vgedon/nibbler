#ifndef APPLE_OBJECT
#define APPLE_OBJECT

#include <chrono>
#include <Point.hpp>
#include <iostream>
#include <define.hpp>

class AppleObject : public Point
{
private:
	unsigned int		 						_lifeTime;
	std::chrono::steady_clock::time_point 		_spawnTime;

public:
					AppleObject();
					AppleObject(const AppleObject & src);
					AppleObject(Point & p, unsigned int lifeTime);
					AppleObject(unsigned int x, unsigned int y, unsigned int lifeTime);
	AppleObject &	operator=(const AppleObject & src);
	virtual 		~AppleObject();


	const std::chrono::steady_clock::time_point	& 	getSpawnTime() const ;
	const unsigned int &							getLifeTime() const;
	bool		 									isAlive();

};

std::ostream &	operator<<(std::ostream & o, AppleObject const & e);

#endif